package de.wumpitz.nemoy;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.wumpitz.nemoy.application.MonthPagerAdapter;
import de.wumpitz.nemoy.application.NemoyApplication;

public class MainActivity extends AppCompatActivity {
    private ViewPager mMonthPage;
    private TextView currentMonthView;
    private ImageView previousMonthView;
    private ImageView nextMonthView;
    private MonthPagerAdapter monthPagerAdapter;

    private Calendar mCurMonth;
    private DateFormat formatMonth = new SimpleDateFormat("MMMM yy", Locale.GERMAN);
    final OutgoingsListView[] mFragList = new OutgoingsListView[3];

    private static final int PAGE_MIDDLE = 1;
    private int mSelectedPageIndex = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent createIntent = new Intent(this, CreateActivity.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        currentMonthView = (TextView) findViewById(R.id.current_month);
        previousMonthView = (ImageView) findViewById(R.id.month_arrow_left);
        nextMonthView = (ImageView) findViewById(R.id.month_arrow_right);

        mCurMonth = Calendar.getInstance();
        Calendar prevMonth, nextMonth;
        prevMonth = (Calendar) mCurMonth.clone();
        nextMonth = (Calendar) mCurMonth.clone();
        prevMonth.add(Calendar.MONTH, - 1);
        nextMonth.add(Calendar.MONTH, 1);
        mFragList[0] = OutgoingsListView.newInstance(prevMonth, false);
        mFragList[1] = OutgoingsListView.newInstance(mCurMonth, true);
        mFragList[2] = OutgoingsListView.newInstance(nextMonth, false);

        mMonthPage = (ViewPager) findViewById(R.id.pager);
        monthPagerAdapter = new MonthPagerAdapter(getSupportFragmentManager(), mFragList);
        mMonthPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int arg0) {
                if (arg0 == ViewPager.SCROLL_STATE_IDLE) {
                    if (mSelectedPageIndex < PAGE_MIDDLE) {
                        mCurMonth.add(Calendar.MONTH, -1);
                        updateDateView();
                        for (OutgoingsListView frag : mFragList) {
                            frag.onPreviousMonth();
                        }
                        mFragList[1].refresh(true);
                    } else if (mSelectedPageIndex > PAGE_MIDDLE) {
                        mCurMonth.add(Calendar.MONTH, 1);
                        updateDateView();
                        for (OutgoingsListView frag : mFragList) {
                            frag.onNextMonth();
                        }
                        mFragList[1].refresh(true);
                    }
                    mMonthPage.setCurrentItem(1, false);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int arg0) {
                mSelectedPageIndex = arg0;
                updateDateView();
            }
        });
        mMonthPage.setAdapter(monthPagerAdapter);
        mMonthPage.setCurrentItem(1, false);

        previousMonthView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMonthPage.setCurrentItem(mMonthPage.getCurrentItem() - 1, true);
            }
        });

        nextMonthView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMonthPage.setCurrentItem(mMonthPage.getCurrentItem() + 1, true);
            }
        });

        setSupportActionBar(toolbar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(createIntent);
            }
        });
    }

    @Override
    public void onResume() {
        // If data has changed (e.g. new Outgoing was created) refresh the View
        NemoyApplication app = ((NemoyApplication) getApplication());
        if (app.hasDataChanged()) {
            app.setDataChanged(false);
            mFragList[1].refresh(true);
        }
        super.onResume();
    }

    private void updateDateView() {
        currentMonthView.setText(formatMonth.format(mCurMonth.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
