package de.wumpitz.nemoy;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Calendar;

import de.wumpitz.nemoy.application.NemoyApplication;
import de.wumpitz.nemoy.bindings.NemoyApi;
import de.wumpitz.nemoy.bindings.NemoyService;
import de.wumpitz.nemoy.bindings.models.Outgoing;
import retrofit2.Response;

public class CreateActivity extends AppCompatActivity {

    private SimpleDateFormat dateFormatter;
    private DatePickerDialog datePickerDialog;

    private EditText dateEtxt;

    private final String LOG_TAG = CreateActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        Calendar newCalendar = Calendar.getInstance();

        Button createButton = (Button) findViewById(R.id.createButton);
        Button cancelButton = (Button) findViewById(R.id.cancelButton);

        dateEtxt = (EditText) findViewById(R.id.date_spent);
        dateEtxt.setInputType(InputType.TYPE_NULL);
        dateFormatter = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);

        dateEtxt.setText(dateFormatter.format(newCalendar.getTime()));

        datePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        dateEtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CreateOutgoingTask().execute(getApplicationContext());
                // Set flag that the outgoing data has changed
                NemoyApplication app = ((NemoyApplication)getApplication());
                app.setDataChanged(true);
            }
        });
    }

    private class CreateOutgoingTask extends AsyncTask<Context, Void, Response> {

        private final String LOG_TAG = CreateOutgoingTask.class.getSimpleName();

        private Outgoing buildOutgoing() {
            EditText amountView = (EditText) findViewById(R.id.amount);
            Float amount = Float.parseFloat(amountView.getText().toString());
            EditText tagsView = (EditText) findViewById(R.id.tags);
            String tags = tagsView.getText().toString();

            Date date;
            try {
                date = dateFormatter.parse(dateEtxt.getText().toString());
            } catch (ParseException e) {
                Toast.makeText(CreateActivity.this, "Wrong date format.!", Toast.LENGTH_SHORT).show();
                return null;
            }
            Outgoing outgoing = new Outgoing();
            outgoing.amount = amount;
            outgoing.date_spent = date;
            outgoing.tags = Arrays.asList(tags.split(" "));
            return outgoing;
        }

        protected Response doInBackground(Context... params) {
            Context context = params[0];
            // Build outgoing
            Outgoing outgoing = buildOutgoing();
            // Get host from preferences
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
            String host = sharedPref.getString("pref_host", "");
            Log.d(LOG_TAG, "Host: " + host);
            // Initialise Nemoy API
            NemoyApi api = new NemoyApi(host);
            NemoyService service = api.getService();

            // Create outgoing
            Response<Outgoing> response;
            try {
                response = service.createOutgoing(outgoing).execute();
            } catch (IOException e) {
                Log.d(LOG_TAG, "Exception " + e.toString());
                return null;
            }
            return response;
        }

        protected void onPostExecute(Response response) {
            if (response == null) {
                Toast.makeText(CreateActivity.this, "A network problem occured. Please retry.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (response.code() != 201) {
                Toast.makeText(CreateActivity.this, "Outgoing not created!", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(CreateActivity.this, "Outgoing created.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
