package de.wumpitz.nemoy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.wumpitz.nemoy.bindings.models.Outgoing;


public class OutgoingArrayAdapter extends ArrayAdapter<Outgoing> {
    private final Context context;
    private final ArrayList<Outgoing> values;

    public OutgoingArrayAdapter(Context context, ArrayList<Outgoing> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_outgoing, parent, false);
        //View rowView = convertView;
        TextView dayView = (TextView) rowView.findViewById(R.id.list_item_outgoing_day);
        TextView tagsView = (TextView) rowView.findViewById(R.id.list_item_outgoing_tags);
        TextView amountView = (TextView) rowView.findViewById(R.id.list_item_outgoing_amount);
        // change the icon for Windows and iPhone
        Outgoing outgoing = values.get(position);
        tagsView.setText(outgoing.tags.toString());
        amountView.setText(String.format(Locale.GERMAN, "%.2f", outgoing.amount));
        Calendar cal = Calendar.getInstance();
        cal.setTime(outgoing.date_spent);
        Integer day = cal.get(Calendar.DAY_OF_MONTH);
        dayView.setText(String.format(Locale.GERMAN, "%02d", day));
        return rowView;
    }
}
