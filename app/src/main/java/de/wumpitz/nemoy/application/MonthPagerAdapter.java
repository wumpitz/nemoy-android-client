package de.wumpitz.nemoy.application;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import de.wumpitz.nemoy.OutgoingsListView;

public class MonthPagerAdapter extends FragmentStatePagerAdapter {
    OutgoingsListView[] fragList;

    public MonthPagerAdapter(FragmentManager fm, OutgoingsListView[] fragList) {
        super(fm);
        this.fragList = fragList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragList[position];
    }

    @Override
    public int getCount() {
        return fragList.length;
    }
}
