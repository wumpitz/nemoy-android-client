package de.wumpitz.nemoy.application;

import android.app.Application;

/**
 * Custom Application to store app wide data.
 */
public class NemoyApplication extends Application {
    private boolean dataChanged = false;

    public boolean hasDataChanged() {
        return dataChanged;
    }

    public void setDataChanged(boolean value) {
        dataChanged = value;
    }
}
