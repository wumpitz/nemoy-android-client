package de.wumpitz.nemoy;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.wumpitz.nemoy.bindings.NemoyApi;
import de.wumpitz.nemoy.bindings.NemoyService;
import de.wumpitz.nemoy.bindings.models.Outgoing;
import de.wumpitz.nemoy.bindings.models.Pager;
import retrofit2.Response;

public class OutgoingsListView extends Fragment {

    private OutgoingArrayAdapter mOutgoingsAdapter;
    private Calendar mCurrentCal;

    private ListView mListContainer;
    private TextView mTotalAmountView;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private SwipeRefreshLayout.OnRefreshListener mSwipeRefreshListner;

    private ArrayList<Outgoing> mToDelete;

    private final String LOG_TAG = RetrieveOutgoingsTask.class.getSimpleName();

    public OutgoingsListView() {
    }

    public Calendar getCurrentCal() {
        return mCurrentCal;
    }

    /**
     * Create an instance of OutgoingsListView
     * @param a Current month as Calendar
     * @param loadOutgoings Load outgoings if true
     * @return OutgoingsListView
     */
    static OutgoingsListView newInstance(Calendar a, boolean loadOutgoings) {
        OutgoingsListView fragment = new OutgoingsListView();
        // Set additional information to pass them to the Fragment
        Bundle args = new Bundle();
        args.putLong("cal", a.getTimeInMillis());
        args.putBoolean("loadOutgoings", loadOutgoings);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Add month to the current one and refresh the view
     */
    protected final void onNextMonth() {
        mCurrentCal.add(Calendar.MONTH, 1);
        refresh(false);
    }

    /**
     * Subtract month to the current one and refresh the view
     */
    protected final void onPreviousMonth() {
        mCurrentCal.add(Calendar.MONTH, -1);
        refresh(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Get additional information
        Bundle args = getArguments();
        Long timeInMillis = args.getLong("cal");
        Boolean loadOutgoings = args.getBoolean("loadOutgoings");

        // Retrieve views
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);
        mListContainer = (ListView) rootView.findViewById(R.id.list_view_outgoings);
        mTotalAmountView = (TextView) rootView.findViewById(R.id.total_amount);

        // Instantiate list container and adapter
        mOutgoingsAdapter = new OutgoingArrayAdapter(getActivity(), new ArrayList<Outgoing>());
        mListContainer.setAdapter(mOutgoingsAdapter);
        mListContainer.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        mToDelete = new ArrayList<>();
        mListContainer.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                if (checked) {
                    mToDelete.add(mOutgoingsAdapter.getItem(position));
                } else {
                    mToDelete.remove(mOutgoingsAdapter.getItem(position));
                }
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        new DeleteOutgoingsTask().execute((ArrayList<Outgoing>) mToDelete.clone());
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate the menu for the CAB
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context_menu, menu);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // Clear the list of outgoings
                mToDelete.clear();
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
        });

        // Instantiate calendar
        mCurrentCal = Calendar.getInstance();
        mCurrentCal.setTimeInMillis(timeInMillis);
        mCurrentCal.set(Calendar.DAY_OF_MONTH, 1);

        // Instantiate SwipeRefreshListener
        mSwipeRefreshListner = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initiateRetrieve();
            }
        };

        mSwipeRefreshLayout.setOnRefreshListener(mSwipeRefreshListner);

        // Finally load outgoings for the given month
        refresh(loadOutgoings);
        return rootView;
    }

    /**
     * Refresh the view. Clears the amount view and the outgoings list. Optionally start the
     * refresh process.
     * @param withOutgoings If true, the refresh process will be started.
     */
    protected void refresh(boolean withOutgoings) {
        mTotalAmountView.setText("");
        // Remove all items from the ListAdapter
        mOutgoingsAdapter.clear();
        if (withOutgoings) {
            mSwipeRefreshLayout.setRefreshing(true);
            mSwipeRefreshListner.onRefresh();
        }
    }

    /**
     * By abstracting the retrieve process to a single method, the app allows both the
     * SwipeRefreshLayout and the Refresh action item to refresh the content.
     */
    private void initiateRetrieve() {
        /*
         * Execute the background task, which uses {@link android.os.AsyncTask} to load the data.
         */
        new RetrieveOutgoingsTask().execute();
    }

    /**
     * When the AsyncTask finishes, it calls onRefreshComplete(), which updates the data in the
     * ListAdapter and turns off the progress bar.
     */
    private void onRefreshComplete(ArrayList<Outgoing> result) {
        // Stop the refreshing indicator
        mSwipeRefreshLayout.setRefreshing(false);
        // Remove all items from the ListAdapter, and then replace them with the new items
        mOutgoingsAdapter.clear();
        if (result.toArray().length == 0) {
            Toast.makeText(getActivity(), "No outgoing found.", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d(LOG_TAG, "Outgoing count: " + result.toArray().length);
        Float total_amount = (float) 0.0;
        for (Outgoing outgoing : result) {
            total_amount += outgoing.amount;
        }
        mOutgoingsAdapter.addAll(result);
        mTotalAmountView.setText(String.format(Locale.GERMAN, "Total %.2f", total_amount));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        // Refresh action manually started
        if (id == R.id.action_refresh) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override public void run() {
                    refresh(true);
                }
            });
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private NemoyService initialiseNemoyService() {
        // Initialise Nemoy API
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String host = sharedPref.getString("pref_host", "");

        NemoyApi api;

        try {
            api = new NemoyApi(host);
        } catch (IllegalArgumentException e) {
            Log.d(LOG_TAG, "Exception " + e.toString());
            return null;
        }
        return api.getService();
    }

    private class DeleteOutgoingsTask extends AsyncTask<ArrayList<Outgoing>, Void, ArrayMap<Outgoing, Boolean>> {

        private final String LOG_TAG = DeleteOutgoingsTask.class.getSimpleName();

        @Override
        protected ArrayMap<Outgoing, Boolean> doInBackground(ArrayList<Outgoing>... params) {
            ArrayList<Outgoing> outgoings = params[0];
            ArrayMap<Outgoing, Boolean> resultMap = new ArrayMap<>(outgoings.size());

            Log.d(LOG_TAG, "Start to delete outgoings: " + outgoings.size());

            NemoyService nemoyService = initialiseNemoyService();
            if (nemoyService == null) {
                return null;
            }
            for(Outgoing outgoing : outgoings) {
                try {
                    nemoyService.deleteOutgoing(outgoing._id, outgoing._etag).execute();
                    resultMap.put(outgoing, true);
                    Log.d(LOG_TAG, "Deleted outgoing.");
                } catch (IOException e) {
                    Log.w(LOG_TAG, "Couldn't delete outgoing " + outgoing);
                    resultMap.put(outgoing, false);
                }
            }
            return resultMap;
        }

        protected void onPostExecute(ArrayMap<Outgoing, Boolean> result) {
            if (result == null) {
                Toast.makeText(getActivity(), "A network problem occured. Please retry.", Toast.LENGTH_SHORT).show();
                return;
            }
            refresh(true);
        }
    }

    /**
     * Task for fetching the outgoings from the Nemoy host.
     */
    private class RetrieveOutgoingsTask extends AsyncTask<String, Void, ArrayList<Outgoing>> {

        private final String LOG_TAG = RetrieveOutgoingsTask.class.getSimpleName();

        private NemoyService mNemoyService;

        protected ArrayList<Outgoing> doInBackground(String... garbage) {
            mNemoyService = initialiseNemoyService();
            if(mNemoyService == null) {
                return null;
            }

            String sortBy = "-date_spent";
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
            String startDateString = format.format(getCurrentCal().getTime());
            Calendar endDate = (Calendar) getCurrentCal().clone();
            endDate.add(Calendar.MONTH, 1);
            String endDateString = format.format(endDate.getTime());
            String whereQueryString = "{\"date_spent\": {\"$gte\": \"%s\", \"$lt\": \"%s\"}}";
            String whereQuery = String.format(whereQueryString, startDateString, endDateString);

            // Get outgoing
            Pager<Outgoing> outgoingsPager;
            try {
                Response<Pager<Outgoing>> pager = mNemoyService.getOutgoings(sortBy, whereQuery).execute();
                outgoingsPager = pager.body();
            } catch (IOException e) {
                Log.d(LOG_TAG, "Exception " + e.toString());
                return null;
            }
            return extractOutgoings(outgoingsPager);
        }

        private ArrayList<Outgoing> extractOutgoings(Pager<Outgoing> outgoingsPager) {
            ArrayList<Outgoing> outgoingResult = new ArrayList<>();
            Log.d(LOG_TAG, "Found " + outgoingsPager.toString());
            for (Outgoing outgoing : outgoingsPager._items) {
                outgoingResult.add(outgoing);
                Log.d(LOG_TAG, "Found " + outgoing.date_spent.toString());
            }
            if (outgoingsPager._links != null && outgoingsPager._links.next != null) {
                String href = outgoingsPager._links.next.href;
                Pager<Outgoing> nextOutgoingsPager;
                Log.d(LOG_TAG, "Next page: " + href);
                try {
                    Response<Pager<Outgoing>> pager = mNemoyService.nextPage(href).execute();
                    nextOutgoingsPager = pager.body();
                } catch (IOException e) {
                    Log.d(LOG_TAG, "Exception " + e.toString());
                    return null;
                }
                ArrayList<Outgoing> nextResults = extractOutgoings(nextOutgoingsPager);
                if (nextResults != null) {
                    outgoingResult.addAll(nextResults);
                }
            }
            return outgoingResult;
        }

        protected void onPostExecute(ArrayList<Outgoing> result) {
            if (result == null) {
                Toast.makeText(getActivity(), "A network problem occured." +
                        " Please retry or check your settings.", Toast.LENGTH_SHORT).show();
                return;
            }
            onRefreshComplete(result);
        }
    }
}
